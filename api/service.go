package api

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/chespinoza/sample-uservice/natsclient"
	"gitlab.com/chespinoza/sample-uservice/user"
)

// Service structure
type Service struct {
	router *chi.Mux
}

// Start start http service
func (s *Service) Start(addr string, userStore *user.Store, natsClient *natsclient.Service) error {
	s.router = chi.NewRouter()

	s.router.Use(middleware.WithValue("USERSTORE", userStore))
	s.router.Use(middleware.WithValue("NATSCLIENT", natsClient))

	s.router.Post("/api/v1/user", newUserHandler)
	s.router.Put("/api/v1/user", updateUserHandler)
	s.router.Delete("/api/v1/user", deleteUserHandler)
	s.router.Get("/api/v1/user", getUserHandler)
	s.router.Get("/api/v1/user/search", searchHandler)
	s.router.Get("/health", healthHandler)

	return http.ListenAndServe(addr, s.router)
}

//New returns a new api service instance
func New() *Service {
	s := new(Service)
	return s
}
