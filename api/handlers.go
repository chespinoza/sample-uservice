package api

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/chespinoza/sample-uservice/natsclient"
	"gitlab.com/chespinoza/sample-uservice/user"
)

func newUserHandler(w http.ResponseWriter, r *http.Request) {
	userStore := r.Context().Value("USERSTORE").(*user.Store)
	u := user.User{}

	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	userID, err := userStore.New(&u)
	if err != nil {
		log.Println("error creating user:", err.Error())
		writeJSON(w, http.StatusInternalServerError, struct {
			Result string `json:"result"`
		}{err.Error()})
		return
	}

	natsClient := r.Context().Value("NATSCLIENT").(*natsclient.Service)
	err = natsClient.PublishMessage("UserCreated", u)
	if err != nil {
		log.Println("error publishing to NATS:", err.Error())
	}

	writeJSON(w, http.StatusOK, struct {
		Result string `json:"result"`
		UserID uint64 `json:"user_id"`
	}{"ok", userID})
}

func updateUserHandler(w http.ResponseWriter, r *http.Request) {
	userStore := r.Context().Value("USERSTORE").(*user.Store)
	u := user.User{}

	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err := userStore.Update(u.ID, &u)
	if err != nil {
		log.Println("error updating user:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, http.StatusInternalServerError, struct {
			Result string `json:"result"`
		}{err.Error()})
		return
	}

	natsClient := r.Context().Value("NATSCLIENT").(*natsclient.Service)
	err = natsClient.PublishMessage("UserUpdated", u)
	if err != nil {
		log.Println("error publishing to NATS:", err.Error())
	}

	writeJSON(w, http.StatusOK, struct {
		Result string `json:"result"`
	}{"ok"})
}

func deleteUserHandler(w http.ResponseWriter, r *http.Request) {
	userStore := r.Context().Value("USERSTORE").(*user.Store)
	u := struct {
		UserID uint64 `json:"user_id"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err := userStore.Delete(u.UserID)
	if err != nil {
		log.Println("error deleting user:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, http.StatusInternalServerError, struct {
			Result string `json:"result"`
		}{err.Error()})
		return
	}

	natsClient := r.Context().Value("NATSCLIENT").(*natsclient.Service)
	err = natsClient.PublishMessage("UserDeleted", u)
	if err != nil {
		log.Println("error publishing to NATS:", err.Error())
	}

	writeJSON(w, http.StatusOK, struct {
		Result string `json:"result"`
	}{"ok"})
}

func getUserHandler(w http.ResponseWriter, r *http.Request) {
	userStore := r.Context().Value("USERSTORE").(*user.Store)
	u := struct {
		UserID uint64 `json:"user_id"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, userObj, err := userStore.GetByID(u.UserID)
	if err != nil {
		log.Println("error getting user:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, http.StatusOK, struct {
			Result string `json:"result"`
		}{err.Error()})
		return
	}
	writeJSON(w, http.StatusOK, struct {
		Result string     `json:"result"`
		User   *user.User `json:"user"`
	}{"ok", userObj})
}

func searchHandler(w http.ResponseWriter, r *http.Request) {
	userStore := r.Context().Value("USERSTORE").(*user.Store)
	u := struct {
		Field string `json:"field"`
		Value string `json:"value"`
	}{}

	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		log.Println("error decoding JSON payload:", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, users := userStore.Search(u.Field, u.Value)
	writeJSON(w, http.StatusOK, struct {
		Result string       `json:"result"`
		Users  []*user.User `json:"users"`
	}{"ok", users})
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	writeJSON(w, http.StatusOK, struct {
		Status string `json:"status"`
	}{"ok"})
}
