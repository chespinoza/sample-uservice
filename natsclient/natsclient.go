package natsclient

import (
	"encoding/json"
	"time"

	"github.com/nats-io/go-nats"
	"github.com/satori/go.uuid"
)

/*
	Nats client
*/

// Service Nats service
type Service struct {
	conn *nats.Conn
}

// NatsMessage Structure to represent
type NatsMessage struct {
	UID       string // Message ID
	Kind      string // [UserCreated, UserUpdated, UserDeleted]
	Body      string // Body containing json data
	CreatedAt time.Time
}

// New returns a new instance of Nats client
func New(url string) (*Service, error) {

	conn, err := nats.Connect(url)
	if err != nil {
		return nil, err
	}
	return &Service{conn: conn}, nil
}

// Close close Nats connection
func (e *Service) Close() {
	if e.conn != nil {
		e.conn.Close()
	}
}

// PublishMessage to nats streaming
func (e *Service) PublishMessage(kind string, obj interface{}) (err error) {

	var msgBytes []byte
	var bodyBytes []byte

	msg := new(NatsMessage)

	// Generate UIID
	uuid := uuid.NewV4()

	msg.Kind = kind
	msg.UID = uuid.String()

	//Marshal obj into message body
	bodyBytes, err = json.Marshal(obj)
	if err != nil {
		return err
	}

	msg.Body = string(bodyBytes)
	msg.CreatedAt = time.Now()

	// Marshal message
	msgBytes, err = json.Marshal(msg)
	if err != nil {
		return err
	}

	e.conn.Publish(msg.Kind, msgBytes)
	if err != nil {
		return err
	}
	return nil
}
