package main

import (
	"log"
	"os"

	"gitlab.com/chespinoza/sample-uservice/api"
	"gitlab.com/chespinoza/sample-uservice/natsclient"
	"gitlab.com/chespinoza/sample-uservice/user"
)

func main() {
	userStore := user.New()

	natsClient, err := natsclient.New(os.Getenv("NATS_SERVER"))
	if err != nil {
		log.Fatal(err)
	}

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	//health.Register("httpChecker", checks.HTTPChecker("http://localhost:8080/health", http.StatusOK, time.Second*5, nil))

	webAPI := api.New()

	log.Println("Starting sample microservice")

	if err := webAPI.Start(":8080", userStore, natsClient); err != nil {
		log.Fatal(err)
	}
}
