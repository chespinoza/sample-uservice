package user

import (
	"errors"
	"net/mail"
	"reflect"

	"golang.org/x/crypto/bcrypt"
)

// User data abstraction
type User struct {
	ID             uint64
	FirstName      string `json:"first_name" db:"first_name"`
	LastName       string `json:"last_name" db:"last_name"`
	NickName       string `json:"nickname" db:"nickname"`
	Password       string `json:"password"`
	hashedPassword []byte `db:"hashed_password"`
	Email          string `json:"email" db:"email"`
	Country        string `json:"country" db:"country"`
}

// Store user storage
type Store struct {
	Users []*User
}

//isValid implements basic fields validation for User abstraction
func (u User) isValid() (isValid bool, err error) {

	if _, err := mail.ParseAddress(u.Email); err != nil {
		return false, errors.New("invalid email")
	}
	if len(u.FirstName) == 0 {
		return false, errors.New("empty field first name ")
	}
	if len(u.LastName) == 0 {
		return false, errors.New("empty field first name ")
	}
	if len(u.Country) == 0 {
		return false, errors.New("empty field country")
	}
	if len(u.NickName) < 3 {
		return false, errors.New("not valid nickname")
	}
	if len(u.Password) <= 8 {
		return false, errors.New("password too short")
	}
	return true, nil
}

// New adds a new user
func (s *Store) New(user *User) (userID uint64, err error) {

	if valid, err := user.isValid(); !valid && err != nil {
		return 0, err
	}

	//check if user already exists
	if matches, _ := s.Search("Email", user.Email); matches != 0 {
		return 0, errors.New("email in use")
	}
	if matches, _ := s.Search("NickName", user.NickName); matches != 0 {
		return 0, errors.New("nickname in use")
	}

	id := uint64(len(s.Users) + 1)
	user.ID = id
	// hashed password
	user.hashedPassword, err = bcrypt.GenerateFromPassword([]byte(user.Password), 10)
	if err != nil {
		return 0, err
	}
	user.Password = ""

	s.Users = append(s.Users, user)

	return id, nil
}

// GetByID gets an user by ID
// Returns index in slice, *User and error
func (s *Store) GetByID(id uint64) (int, *User, error) {

	for i := 0; i < len(s.Users); i++ {
		if s.Users[i].ID == id {
			return i, s.Users[i], nil
		}
	}
	return 0, nil, errors.New("Not found")
}

// Delete deletes an user by ID
func (s *Store) Delete(id uint64) error {
	index, _, err := s.GetByID(id)
	if err != nil {
		return err
	}
	s.Users = append(s.Users[:index], s.Users[index+1:]...)

	return nil
}

// Update updates an user by ID
func (s *Store) Update(id uint64, user *User) error {
	index, _, err := s.GetByID(id)
	if err != nil {
		return err
	}
	if valid, err := user.isValid(); !valid && err != nil {
		return err
	}

	// hashed password
	user.hashedPassword, err = bcrypt.GenerateFromPassword([]byte(user.Password), 10)
	if err != nil {
		return err
	}
	user.Password = ""

	s.Users[index] = user

	return nil
}

// Search searches and returns a list of users
func (s *Store) Search(fieldName, value string) (matches int, users []*User) {

	for i := 0; i < len(s.Users); i++ {
		val := reflect.ValueOf(s.Users[i])
		field := reflect.Indirect(val).FieldByName(fieldName)
		if field.IsValid() {
			if field.Interface() == value {
				users = append(users, s.Users[i])
				matches++
			}
		}
	}
	if matches == 0 {
		return 0, nil
	}

	return matches, users
}
