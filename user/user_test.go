package user

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

func setupTest() *Store {
	store := &Store{}
	_, err := store.New(&User{
		FirstName: "Peter",
		LastName:  "Parker",
		NickName:  "Spiderman",
		Password:  "s3cr3t&spyd3r&p4ss",
		Email:     "spider@web.com",
		Country:   "Chile",
	})
	if err != nil {
		panic(err)
	}
	_, err = store.New(&User{
		FirstName: "Pat",
		LastName:  "Park",
		NickName:  "Goist",
		Password:  "oinefef%6dio%$deaW",
		Email:     "park@web.com",
		Country:   "Germany",
	})
	if err != nil {
		panic(err)
	}
	_, err = store.New(&User{
		FirstName: "Pedro",
		LastName:  "Pedreros",
		NickName:  "Furia99",
		Password:  "Ew3de%efrr12344E4%",
		Email:     "pedro@web.com",
		Country:   "Chile",
	})
	if err != nil {
		panic(err)
	}

	return store
}

func TestNewUser(t *testing.T) {
	store := setupTest()

	userID, err := store.New(&User{
		FirstName: "Tony",
		LastName:  "Stark",
		NickName:  "Ironman",
		Password:  "Ir0n&P4zz",
		Email:     "tony@starktech.com",
		Country:   "Germany",
	})

	assert.Nil(t, err)
	// User number 4
	assert.Equal(t, uint64(4), userID)
	assert.NotNil(t, store.Users)
	assert.Len(t, store.Users, 4)
}

func TestGetUser(t *testing.T) {
	store := setupTest()

	_, _, err := store.GetByID(0)
	assert.NotNil(t, err)

	_, user, err := store.GetByID(1)
	assert.Nil(t, err)
	assert.Equal(t, uint64(1), user.ID)
	assert.Equal(t, "spider@web.com", user.Email)

}

func TestDeleteUser(t *testing.T) {
	store := setupTest()

	err := store.Delete(1)
	assert.Nil(t, err)
	log.Println(store.Users)
	assert.Len(t, store.Users, 2)

	_, user, err := store.GetByID(1)
	assert.Nil(t, user)
	assert.NotNil(t, err)

	assert.Len(t, store.Users, 2)
}

func TestUpdateUser(t *testing.T) {
	users := setupTest()

	_, user, err := users.GetByID(1)
	assert.Nil(t, err)
	user.Email = "spiderman@web.com"
	user.Country = "USA"
	user.FirstName = "Ron"
	user.LastName = "J Fortier"
	user.NickName = "BruceLee"
	user.Password = "MynewCrazyP455w0rdZ"

	err = users.Update(1, user)
	assert.Nil(t, err)

	_, updatedUser, err := users.GetByID(1)
	assert.Nil(t, err)
	assert.Equal(t, "spiderman@web.com", updatedUser.Email)
}

func TestSearchUsers(t *testing.T) {
	store := setupTest()

	results, users := store.Search("Country", "Chile")
	assert.Equal(t, 2, results)
	assert.Len(t, users, 2)

	results, users = store.Search("Country", "Italy")
	assert.Equal(t, 0, results)
	assert.Len(t, users, 0)

	results, users = store.Search("Country", "Germany")
	assert.Equal(t, 1, results)
	assert.Len(t, users, 1)
}
