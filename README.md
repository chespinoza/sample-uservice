# Sample microservice

![](screenshoot.png)

## How to run
In one terminal:

`make build-compose`

then

`make run` 

In Another Terminal to run a NATS subscriber:

`go run cmd/client/main.go UserCreated`


And finally the next commands in  another terminal:

### Create a new user

`curl -i -H "Content-type:application/json" -X POST http://localhost:8080/api/v1/user -d '{"first_name":"Sam", "last_name":"Dom", "nickname":"sam", "password":"secretapassword", "email":"sam@domain.com", "country":"Chile"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Sun, 05 Aug 2018 22:03:56 GMT
Content-Length: 27

{"result":"ok","user_id":1}% 
```

### Update user:

`curl -i -H "Content-type:application/json" -X PUT http://localhost:8080/api/v1/user -d '{"id":1,"first_name":"Sam", "last_name":"Dom", "nickname":"sam", "password":"secretapassword", "email":"sam@domain.com", "country":"China"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Sun, 05 Aug 2018 22:04:24 GMT
Content-Length: 15

{"result":"ok"}%
```

### Get user:

`curl -i -H "Content-type:application/json" -X GET http://localhost:8080/api/v1/user -d '{"user_id":1}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Sun, 05 Aug 2018 22:04:53 GMT
Content-Length: 142

{"result":"ok","user":{"ID":1,"first_name":"Sam","last_name":"Dom","nickname":"sam","password":"","email":"sam@domain.com","country":"China"}}%  
```

### Delete user:

`curl -i -H "Content-type:application/json" -X DELETE http://localhost:8080/api/v1/user -d '{"user_id":1}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Sun, 05 Aug 2018 22:05:38 GMT
Content-Length: 15

{"result":"ok"}%  
```

### Search users:

**Be sure before to run the search that are users registered in the system

`curl -i -H "Content-type:application/json" -X GET http://localhost:8080/api/v1/user/search -d '{"field":"Country", "value":"Chile"}'`

```
HTTP/1.1 200 OK
Content-Type: application/json
Date: Sun, 05 Aug 2018 22:07:27 GMT
Content-Length: 267

{"result":"ok","users":[{"ID":1,"first_name":"Sam","last_name":"Dom","nickname":"sam","password":"","email":"sam@domain.com","country":"Chile"},{"ID":2,"first_name":"Sam","last_name":"Dom","nickname":"sam1","password":"","email":"sam1@domain.com","country":"Chile"}]}%
```
