FROM golang:alpine AS builder
RUN apk --no-cache add \
    build-base \
    bash \
    git
ADD . /go/src/gitlab.com/chespinoza/sample-uservice
WORKDIR /go/src/gitlab.com/chespinoza/sample-uservice
RUN echo 'Building binary with: ' && go version
RUN make build

FROM alpine
RUN apk --no-cache add \
    ca-certificates
WORKDIR /app
EXPOSE 8080
COPY --from=builder /go/src/gitlab.com/chespinoza/sample-uservice/build/sample-uservice /app/
CMD /app/sample-uservice